﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using KdeConnectEarly.Core;
using KdeConnectEarly.Core.Backends;
namespace GraphicsTest
{
    internal abstract class Test : ICommand
    {

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged;

        public abstract void Execute(object parameter);
    }

    internal class LanTest : Test
    {
        public override void Execute(object parameter)
        {
            
        }
    }

    class TestCollection : ObservableCollection<Test>
    {
    }
}
