﻿
using System;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Core;
using Windows.Networking.Sockets;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Parameters;

namespace KdeConnectEarly.Core.Backends.Lan
{
    /// <summary>
    /// Represents device link over LAN network. Essential for device class
    /// <seealso cref="Device"/>
    /// </summary>
    public class LanLink : Link, IDisposable
    {
        private const int port = 1714;
        private SocketLineReader socket;

        public event EventHandler<DisconnectEventArgs> linkLost; 

        public LanLink(string deviceId, LanLinkProvider provider, StreamSocket socket)
            : base(deviceId, provider)
        {
            this.socket = new SocketLineReader(socket);
            this.socket.error += linkLost;
            this.socket.messageReceived += messageReceived;
        }

        /// <summary>
        /// Obtain a encrypted package and decrypt it with my private key
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void messageReceived(object sender, MessageEventArgs e)
        {
            
        }

        public override async Task<bool> sendPackage(NetworkPackage package)
        {
            if (package.hasPayload())
                throw new NotImplementedException();
            try
            {
                
                return await socket.write(
                    Encoding.UTF8.GetBytes(
                        NetworkPackage.serialize(package)));
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exception catched at LanLink::sendPackage: {0}", ex.StackTrace);
                return false;
            }
        }


        public override async Task<bool> sendEncryptedPackage(NetworkPackage package, RsaKeyParameters pubKey)
        {
            if (package.hasPayload())
                throw new NotImplementedException();
            try
            {
                var encrypted = NetworkPackage.encrypt(package, pubKey);
                return await socket.write(Encoding.UTF8.GetBytes(
                    NetworkPackage.serialize(encrypted)));
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exception catched at LanLink::sendEncryptedPackage: {0}", ex.StackTrace);

                return false;
            }
        }

        public void Dispose()
        {
            socket.Dispose();
        }
    }
}