﻿using System;
using System.Threading.Tasks;
using Org.BouncyCastle.Crypto.Parameters;

namespace KdeConnectEarly.Core.Backends
{
    /// <summary>
    /// Represents basic device link. Reimplemented in LAN and Loopback
    /// </summary>
    public abstract class Link
    {

        private readonly LinkProvider m_provider;
        public LinkProvider provider { get { return m_provider; } }

        private readonly string m_deviceId;

        public string deviceId { get { return m_deviceId;} }

        public RsaPrivateCrtKeyParameters privateKey { get; set; }

        /// <summary>
        /// For received packages
        /// </summary>
        /// <param name="package">Received package</param>
        public delegate void PackageReceivedHandler(NetworkPackage package);

        public event PackageReceivedHandler PackageReceived;

        /// <summary>
        /// Call this from subclasses when package is received
        /// </summary>
        /// <param name="package"></param>
        protected void onPackageReceived(NetworkPackage package)
        {
            if (PackageReceived == null) //no subscribers
                return;
            PackageReceived(package);
        }

        protected Link(string deviceId, LinkProvider provider)
        {
            this.m_provider = provider;
            this.m_deviceId = deviceId;
        }

        public abstract Task<bool> sendPackage(NetworkPackage package);
        public abstract Task<bool> sendEncryptedPackage(NetworkPackage package, RsaKeyParameters pubKey);


    }
}