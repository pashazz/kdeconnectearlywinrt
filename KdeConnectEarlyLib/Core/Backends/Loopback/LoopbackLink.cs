﻿
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Org.BouncyCastle.Crypto.Parameters;


namespace KdeConnectEarly.Core.Backends.Loopback
{
    public class LoopbackLink : Link
    {
        public LoopbackLink(LinkProvider provider) :
            base("loopback", provider)
        {
        }

        public override async Task<bool> sendPackage(NetworkPackage package)
        {
            return await Task.Run(() =>
            {
                string s =
                    NetworkPackage.serialize(package);
                NetworkPackage outPackage = NetworkPackage.unserialize(s);
                if (package.hasPayload())
                    outPackage.setPayload(package.payload, package.payloadSize);
                onPackageReceived(outPackage);
                return true; //everything is good always
            });
        }

        public override async Task<bool> sendEncryptedPackage(NetworkPackage package, RsaKeyParameters pubKey)
        {
            return await Task.Run(() =>
            {
                try
                {
                    package = NetworkPackage.encrypt(package, pubKey);
                    string s = NetworkPackage.serialize(package);
                    NetworkPackage outPackage = NetworkPackage.unserialize(s);
                    outPackage = NetworkPackage.decrypt(outPackage, privateKey);
                    onPackageReceived(outPackage);
                    if (package.hasPayload())
                        outPackage.setPayload(package.payload, package.payloadSize);
                    return true;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Exception: " + ex.StackTrace);
                    return false;
                }
            });
        }
    }
}