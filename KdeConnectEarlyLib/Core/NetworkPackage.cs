﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using System.Text;
using Windows.Devices.Input;
using Windows.Security.ExchangeActiveSyncProvisioning;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Encodings;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Crypto.Parameters;

namespace KdeConnectEarly.Core
{
    /// <summary>
    ///NetworkPackage is a package sent thru Net
    ///if it carries a file, it is reflected in payloadSize and payloadTransferInfo properties
    /// This class is intended for JSON Serialization 
    /// Properties marked protected may be set by DataContractJsonSerializer
    /// Properties marked public may be set by user
    /// </summary>   

    [DataContract]
    public class NetworkPackage
    {
        //bytes in 1 chunk encrypted
        private const int ChunkSize = 128;
        private const int ProtocolVersion = 5;
 

        public static void getDeviceInfo(out string name, out string id)
        {
            name = "Windows phone";
            id = "1995";
        }

        public NetworkPackage(string packageType = "")
        {
            id = getUniqueId();
            type = packageType;
            body = new Dictionary<string, object>();
            payload = null;
            payloadSize = 0;

        }

        public bool hasPayload()
        {
            return payload != null;
        }

        private NetworkPackage()
        {
            throw new NotImplementedException();
        }

        [DataMember(IsRequired = true)] //indicates that this member is serialized
        public long id { get; set; }

        [DataMember(IsRequired = true)]
        public string type { get; set; }

        ///<summary>
        ///This is an dictionary of objects that should be sent with this Package
        ///</summary>
        [DataMember(IsRequired = true)]
        public Dictionary<string, object> body { get; protected set; }

        /// <summary>
        /// This is the size of carried file. Set through setPayload
        /// -1 means endless size
        /// </summary>
        [DataMember(IsRequired = false, EmitDefaultValue = false)]
        //Indicates that default value should not be serialized
        public long payloadSize { get; protected set; }

        /// <summary>
        /// Information about carried file
        /// To be set by DeviceLink
        /// </summary>
        [DataMember(IsRequired = false, EmitDefaultValue = false)]
        public Dictionary<String, object> payloadTransferInfo { get; set; }


        /// <summary>
        /// Payload. To set, use setPayload method
        /// </summary>
        public Stream payload { get; protected set; //not for json though
        }

        public void setPayload(Stream payload, long payloadSize)
        {
            if (payloadSize < -1)
                throw new ArgumentOutOfRangeException("payloadSize", "Payload size must be non-negative or -1 if endless");


            this.payload = payload;
            this.payloadSize = payloadSize;
        }

        /// <summary>
        /// Get value from body attribute
        /// </summary>
        /// <typeparam name="T">Type of the attibute</typeparam>
        /// <param name="key">Key</param>
        /// <param name="defaultValue"></param>
        /// <returns>Value from body if exists. Else defaultValue</returns>
        public T get<T>(string key, T defaultValue)
        {
            object o;
            if (body.TryGetValue(key, out o))
                return (T) o;
            else
                return defaultValue;
        }

        /// <summary>
        /// Set value for body attribute
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void set<T>(string key, T value)
        {
            body.Add(key, (object) value);
        }

        /// <summary>
        /// Unique ID for NetworkPackage. Ported from QDateTime::currentMSecsSinceEpoch
        /// </summary>
        /// <returns>msecs since epoch (UNIX time)</returns>
        public static long getUniqueId()
        {
            var unixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return (long) ((DateTime.UtcNow - unixEpoch).TotalMilliseconds);
        }

        /// <summary>
        /// device name
        /// </summary>
        /// <returns>
        /// Current device name. Should be impementer in different ways on various platforms
        /// </returns>
        public static string getDeviceName()
        {
            EasClientDeviceInformation deviceInfo = new EasClientDeviceInformation();
            return deviceInfo.SystemSku; //TODO figure out smth better
        }

        
        /// <summary>
        /// creates the identity package for device with name deviceId
        /// </summary>
        /// <param name="np">Package to be created</param>
        /// <param name="deviceId">device ID (set by user)</param>
        /// <param name="deviceType">"phone", "tablet", "desktop"</param>
        public static NetworkPackage createIdentityPackage()
        {
            NetworkPackage np = new NetworkPackage(PackageTypes.PACKAGE_TYPE_IDENTITY);
            np.id = getUniqueId();
            np.payload = null;
            np.payloadSize = 0;
            string name, id;
            getDeviceInfo(out name, out id);

            np.set("deviceId", id);
            np.set("deviceName", name); //maybe getDeviceName() is better?
            np.set("protocolVersion", ProtocolVersion);
            
            //Device type
            EasClientDeviceInformation device = new EasClientDeviceInformation();
            string deviceType;
            if (device.OperatingSystem == "WindowsPhone")
                deviceType = "phone";
            else //Windows
            { //if keyboard is present -> desktop (laptop?)
                deviceType = new KeyboardCapabilities().KeyboardPresent == 0 ? "tablet" : "desktop";
            }
           np.set("deviceType", deviceType);
        

            //TODO implement protocols
            np.set("SupportedIncomingInterfaces", "kdeconnect.ping");
            np.set("SupportedOutgoingInterfaces", "kdeconnect.ping");
            return np;
        }

        public static NetworkPackage unserialize(String json)
        {
            return JsonConvert.DeserializeObject<NetworkPackage>(json);
        }

        public static String serialize(NetworkPackage package)
        {
            return JsonConvert.SerializeObject(package);
        }

        /// <summary>
        /// Decrypt a NetworkPackage
        /// </summary>
        /// <param name="package">package to decrypt</param>
        /// <param name="parameter">Private key</param>
        /// <returns></returns>
        public static NetworkPackage decrypt(NetworkPackage package, RsaPrivateCrtKeyParameters parameter)
        {
            List<string> chunks = package.get("data", new List<string>());
            if (chunks.Count == 0)
                throw new ArgumentException("Non-encrypted package", "package");

            var decryptEngine = new Pkcs1Encoding(new RsaEngine());
            decryptEngine.Init(false, parameter);



            StringBuilder builder = new StringBuilder();


            foreach (var chunk in chunks)
            {

                var encrypted = Convert.FromBase64String(chunk);
                var decrypted = decryptEngine.ProcessBlock(encrypted, 0, encrypted.Length);
                string str = Encoding.UTF8.GetString(decrypted, 0, decrypted.Length);
                builder.Append(str);

            }
            //deserialize
            return unserialize(builder.ToString());
        }

        /// <summary>
        /// Create encrypted package based on given package. This package has a list of base64strings 
        /// ChunkSize bytes each for sending to the client
        /// </summary>
        /// <param name="package">Package to be encrypted</param>
        /// <param name="parameter">Client's public key</param>
        /// <returns></returns>
        public static NetworkPackage encrypt(NetworkPackage package, RsaKeyParameters parameter)
        {
            NetworkPackage np;
            String serialized = NetworkPackage.serialize(package);

            List<String> chunks = new List<string>();

            var bytes = Encoding.UTF8.GetBytes(serialized);
            //Initialize cipher


            var encryptEngine = new Pkcs1Encoding(new RsaEngine());
            encryptEngine.Init(true, parameter);

            for (int pos = 0; pos < bytes.Length; pos += ChunkSize)
            {
                int len;
                if (bytes.Length - pos < ChunkSize)
                    len = bytes.Length - pos;
                else
                    len = ChunkSize;
                var encrypted = encryptEngine.ProcessBlock(bytes, pos, len);
                chunks.Add(Convert.ToBase64String(encrypted));
            }

            np = new NetworkPackage(PackageTypes.PACKAGE_TYPE_ENCRYPTED);
            np.set("data", chunks);

            return np;

        }

        [OnDeserialized]
        void onDeserialized(StreamingContext context)
        { //set payload size according to body, if -1
            if (payloadSize == -1)
            { //TODO: Cover it with new test
                payloadSize = get("size", -1);
            }
        }

    }
}
