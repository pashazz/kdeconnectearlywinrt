﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Windows.Networking.Sockets;
using Windows.Storage.Streams;

namespace KdeConnectEarly.Core
{
    public class DisconnectEventArgs : EventArgs
    {
        public DisconnectEventArgs(SocketErrorStatus status)
        {
            this.status = status;
        }
        public SocketErrorStatus status { get; set; }
    }

    public class MessageEventArgs : EventArgs
    {
        public byte[] data { get; set; }
    }
    /// <summary>
    /// Wrapper around windows.networking.sockets for reading line by line
    /// </summary>
    public class SocketLineReader : IDisposable
    {
        public event EventHandler<DisconnectEventArgs> error;
        public event EventHandler<MessageEventArgs> messageReceived;

        private StreamSocket m_socket;
        private DataWriter m_writer;

        public StreamSocketInformation info
        {
            get { return m_socket.Information; }
        }

        public SocketLineReader(StreamSocket socket)
        {
            m_socket = socket;
            if (m_socket == null)
            {
                if (error != null)
                    error(this, new DisconnectEventArgs(0));
            }
            
        }

       

        /// <summary>
        /// Consume TCP stream and emit events when needed
        /// Call this method before writing to stream!
        /// </summary>
        public async Task consumeStream()
        {
            DataReader reader = new DataReader(m_socket.InputStream);
            m_writer = new DataWriter(m_socket.OutputStream);
            try
            {
                reader.InputStreamOptions = InputStreamOptions.Partial;
                List<byte> bytes = new List<byte>();
                while (true)
                {
                    //Read bytes until newline
                    if (await reader.LoadAsync(1) == 0)
                    {//connection closed by peer
                        return;
                    }
                    byte b = reader.ReadByte();
     
                    Debug.WriteLine(Encoding.UTF8.GetChars(new []{b})[0]);
                    bytes.Add(b);
                    //This will generate byte array w/length 1 and then convert it to UTF8 char array and then compare
                    //if (Encoding.UTF8.GetChars(new[] {b})[0] == '\n')
                    if (b == Constants.MessageSeparator) //Unicode for newline
                    {
                        //Create byte array
                        if (bytes.Count == 1)
                        {
                            bytes.Clear();
                            continue; //Do not want a single \n
                        }
                        onMessageReceived(new MessageEventArgs() {data = bytes.ToArray()});
                        bytes.Clear();
                    }
                    
                }
            }
            catch (Exception ex) //TODO catch only n/w exceptions
            {
                 onError(new DisconnectEventArgs(SocketError.GetStatus(ex.HResult)));
                 Debug.WriteLine("Exception catched at consumeStream: {0}", ex.StackTrace);
            }
            finally
            {
                reader.DetachStream();
                m_writer.DetachStream();                
            }
        }

        private void onError(DisconnectEventArgs e)
        {
            if (error != null)
                error(this, e);
        }

        private void onMessageReceived(MessageEventArgs e)
        {
            if (messageReceived != null)
                messageReceived(this, e);
        }

        public async Task<bool> write(byte[] data)
        {
            m_writer.WriteBytes(data);
            //Write end of the line
            m_writer.WriteByte(Constants.MessageSeparator);
            //Try to store the data
            try
            {
                await m_writer.StoreAsync();
                return true;
            }
            catch (Exception ex)
            {
                //Raise onDisconnected
                if (!Enum.IsDefined(typeof (SocketErrorStatus), ex.HResult))
                    throw; //Not a socket error hmmm
                Debug.WriteLine("Exception catched at SocketLineReader::write: {0}", ex.StackTrace);
                onError(new DisconnectEventArgs(SocketError.GetStatus(ex.HResult)));
                
                return false;
            }

            
        }

        public void Dispose()
        {
            m_socket.Dispose();
        }
    }
}